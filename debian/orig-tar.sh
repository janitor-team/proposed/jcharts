#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=jcharts-$2

# clean up the upstream tarball
unzip -d debian/ $3
mv debian/jCharts-$2 debian/$DIR
GZIP=--best tar czf ../jcharts_$2.orig.tar.gz -X debian/orig-tar.exclude -C debian $DIR
rm -rf debian/$DIR
rm -f $3

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
    . .svn/deb-layout
    mv $3 $origDir
    echo "moved $3 to $origDir"
fi

exit 0
